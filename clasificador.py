#!/usr/bin/python3

"""Desarrolla un programa que permita ingresar una cadena
(un string) y evalúe si es un correo electrónico, un entero,
un real u otra cosa.
"""

import sys


def es_correo_electronico(string):
    pos_arroba = string.find('@')
    if pos_arroba == -1:
        return False
    else:
        for i in range(pos_arroba +1, len(string)):
            if string[i] == '.':
                return True
        return False



def es_entero(string):
    try:
        int(string)
        return True
    except ValueError:
        return False

def es_real(entrada):
    try:
        float(entrada)
        return True
    except ValueError:
        return False

def evaluar_entrada(string):
    if string == "":
        print("No has introducido una cadena.")
        return "NONE"
    elif es_correo_electronico(string):
        return "Es un correo electrónico."
    elif es_entero(string):
        return "Es un número entero."
    elif es_real(string):
        return "Es un número real."
    else:
        return "No es ni un correo electrónico, ni un número entero, ni un número real."


def main():
    if len(sys.argv) < 2:
        sys.exit("Error: se espera al menos un argumento")
    string = sys.argv[1]
    resultado = evaluar_entrada(string)
    print(resultado)

if __name__ == '__main__':
    main()

